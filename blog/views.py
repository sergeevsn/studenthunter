from django.shortcuts import render
from .models import Post, Comment
from .models import UserExtendent
from django.utils import timezone
from django.shortcuts import render, get_object_or_404
from .forms import PostForm, SignUpForm, CommentsForm
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login


def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    return render(request, 'blog/post_list.html', {'posts':posts})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})

@login_required
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})

@login_required
def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
           # post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})

@login_required
def deactivate_task(request, pk):
    post = get_object_or_404(Post, pk=pk)
    # post.executor = request.user
    # post.save()
    post.isactive = False
    return redirect('post_list')

@login_required
def exe_add(request, pk):
    post = get_object_or_404(Post, pk=pk)
    # post.executor = request.user
    # post.save()
    post.executor.add(request.user.userextendent)
    return redirect('post_list')

@login_required
def post_draft_list(request):
    posts = Post.objects.filter(published_date__isnull=True).order_by('created_date')
    return render(request, 'blog/post_draft_list.html', {'posts': posts})

@login_required
def post_publish(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.publish()
    return redirect('post_detail', pk=pk)

@login_required
def post_remove(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.delete()
    return redirect('post_list')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.userextendent.desc = form.cleaned_data.get('desc')
            user.userextendent.location = form.cleaned_data.get('location')
            user.userextendent.phone = form.cleaned_data.get('phone')
            user.userextendent.email = form.cleaned_data.get('email')
            user.save()
            my_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=my_password)
            login(request, user)
            return redirect('post_list')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})

def add_comment(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = CommentsForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = CommentsForm()
    return render(request, 'blog/add_comment.html', {'form': form})

def emp_list(request):
    employers = UserExtendent.objects.filter(category='EM')
    return render(request, 'blog/emplist.html', {'employers': employers})

def stud_list(request):
    students = UserExtendent.objects.filter(category='ST')
    return render(request, 'blog/studlist.html', {'students': students})

def stud_profile(request, pk):
    student = get_object_or_404(UserExtendent, pk=pk)
    return render(request, 'blog/studprofile.html', {'student': student})

def employ_profile(request, pk):
    employ = get_object_or_404(UserExtendent, pk=pk)
    return render(request, 'blog/employprofile.html', {'employ': employ})

def about(request):
    about = UserExtendent.objects.filter(category='ST')
    return render(request, 'blog/about.html', {'about': about})

# def employ_profile_list(request):
#     emposts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
#     return render(request, 'blog/employprofile.html', {'emposts':emposts})
