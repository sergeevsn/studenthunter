from django.contrib import admin
from .models import Post, Comment, TaskCategory, UserExtendent

admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(TaskCategory)
admin.site.register(UserExtendent)