from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^graph/$', views.show_graph, name='plot_pic'),
    ]