import io

import numpy as np
from django.http import HttpResponse
from django.shortcuts import render, render_to_response
import matplotlib
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.backends.backend_template import FigureCanvas
from matplotlib.dates import DateFormatter
from matplotlib.figure import Figure
from six import StringIO
import pandas as pd
import seaborn as sns

matplotlib.use('Agg')
import matplotlib.pyplot as plt



# def graph_create(points, title, xlabel, ylabel):
#     fig = Figure(facecolor = 'white', edgecolor = 'white')
#     graph = fig.add_subplot(111)
#     graph.set_title(title)
#     graph.set_xlabel(xlabel)
#     graph.set_ylabel(ylabel)
#
#     graph.grid()
# #    graph.legend(["hello"]) Выдает ошибку Division by Zero
#
#     for x, y, f in points:
#         graph.plot_date(x, y, f) #Если нужны даты по оси x, если обычные данные, используйте graph.plot(x, y, f)
#
#     graph.xaxis.set_major_formatter(DateFormatter("%Y.%m.%d %H:%M")) #Опять же, если даты.
#     fig.autofmt_xdate()
#     output = StringIO.StringIO()
#     canvas = FigureCanvas(fig)
#     canvas.print_png(output)
#     return output.getvalue()
#
# def showimage(request, points, title, xlabel, ylabel):
#     pic = graph_create(points, title, xlabel, ylabel)
#     response = HttpResponse(pic, content_type = 'image/png')
#     return response
#
# def graph(request):
#     x, y = [1, 2, 3], [2, 3, 4]
#     u, v = [-1, -2, -3], [-2, -3, -5]
#     title = u'Пробный график'
#     xlabel = u'Время'
#     ylabel = u'Энергия'
#     points = [(x, y, 'g^'), (u, v, 'ro')]
#     return showimage(request, points, title, xlabel, ylabel)
#
#
#
#
# def plot_pic(request):
#     plt.draw()
#     response = HttpResponse(content_type="image/jpeg")
#     plt.savefig(response, format="png")
#     plt.clf()
#     return response
#
#
# def show_graph(request):
#     return render_to_response("graph/grafik.html", {"plot_pic": graph})


def GraphsViewBar(request):
    f = plt.figure()
    df = pd.read_csv("https://raw.githubusercontent.com/selva86/datasets/master/mpg_ggplot2.csv")

    # Draw Stripplot
    fig, ax = plt.subplots(figsize=(16, 10), dpi=80)
    sns.stripplot(df.cty, df.hwy, jitter=0.25, size=8, ax=ax, linewidth=.5)

    # Decorations
    plt.title('Капща', fontsize=22)

    canvas = FigureCanvasAgg(f)
    buffer = io.BytesIO()
    plt.savefig(buffer, format='png')
    plt.close(f)
    return HttpResponse(buffer.getvalue(), content_type='image/png')